#!/bin/bash

javac `find -name '*.java'` # compilation
rm -f JeuDeLaVie.jar # supp. l'ancien .jar
cd src && jar cfe JeuDeLaVie.jar Main *.class # génère le .jar
cd ..
rm `find -name '*.class'` # supprime les .class
mv `find -name '*.jar'` . # déplace le .jar à la racine
chmod +x *.jar # le rend éxécutable