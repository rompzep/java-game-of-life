/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:19:04 CET 2019
*
*/
public class CelluleEtatMort implements CelluleEtat {
	private static CelluleEtat singleton = new CelluleEtatMort();

	private CelluleEtatMort() {}
	
	public static CelluleEtat getInstance() {
		return singleton;
	}
	
	public CelluleEtat vit() {
		return CelluleEtatVivant.getInstance();
	}
	
	public CelluleEtat meurt() {
		return this;
	}
	
	public Boolean estVivante() {
		return false;
	}

	public void accepte(Visiteur v, Cellule c) {
		v.visiteCelluleMorte(c);	
	}
}