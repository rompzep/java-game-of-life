/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 12:35:55 CEST 2019
*
*/
public class CommandeMeurt extends Commande {
		
	public CommandeMeurt(Cellule cell) {
		this.cell = cell;
	}

	public void executer() {
		cell.meurt();
	}
}