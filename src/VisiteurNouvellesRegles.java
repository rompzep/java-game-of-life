/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 14:06:21 CEST 2019
*
*/
public class VisiteurNouvellesRegles extends Visiteur {
	
	public VisiteurNouvellesRegles(JeuDeLaVie jeu) {
		super(jeu);
	}

	public void visiteCelluleVivante(Cellule c) {
		int nbVois = c.nbVoisinesVivantes(jeu);
		if (nbVois < 3 || nbVois > 8 || nbVois == 5)
			jeu.ajouteCommande(new CommandeMeurt(c));
	}

	public void visiteCelluleMorte(Cellule c) {
		int nbVois = c.nbVoisinesVivantes(jeu);
		if (nbVois == 3 || nbVois > 5 && nbVois < 9)
			jeu.ajouteCommande(new CommandeVit(c));
	}
}