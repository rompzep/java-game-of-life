/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 13:04:41 CEST 2019
*
*/
public class VisiteurClassique extends Visiteur {
	public VisiteurClassique(JeuDeLaVie jeu) {
		super(jeu);
	}

	public void visiteCelluleVivante(Cellule c) {
		int nbVois = c.nbVoisinesVivantes(jeu);
		if (nbVois < 2 || nbVois > 3)
			jeu.ajouteCommande(new CommandeMeurt(c));
	}

	public void visiteCelluleMorte(Cellule c) {
		if (c.nbVoisinesVivantes(jeu) == 3)
			jeu.ajouteCommande(new CommandeVit(c));
	}
}