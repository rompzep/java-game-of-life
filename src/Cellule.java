/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:11:36 CET 2019
*
*/
public class Cellule {
	private CelluleEtat state;
	private int x;
	private int y;

	public Cellule(int x, int y, CelluleEtat state) {
		this.state = state;
		this.x = x;
		this.y = y;
	}

	public int nbVoisinesVivantes(JeuDeLaVie jeu) {
		int nbVois = 0;
		int xMax = jeu.getXMax();
		int yMax = jeu.getYMax();
		
		for (int l=y-1; l<=y+1; l++) {
			for (int c=x-1; c<=x+1; c++) {
				if (l>=0 && l<yMax && c>=0 && c<xMax
					&& !(l==y && c==x)
					&& jeu.getGrille(c,l).estVivante())

					nbVois += 1;
			}	
		}
		return nbVois;
	}

	public void vit() {
		state = state.vit();
	}
	public void meurt() {
		state = state.meurt();
	}
	public Boolean estVivante() {
		return state.estVivante();
	}

	public String toString() {
		return estVivante() ? "o" : " ";
	}

	public void accepte(Visiteur v) {
		state.accepte(v, this);
	}
}