/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Tue Apr 09 10:56:59 CEST 2019
*
*/
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.event.*;
import java.awt.event.*;

public class Main {
	private JeuDeLaVie jeu = new JeuDeLaVie();
	private Boolean isRunning = true;
	private String mode = "classique";
	private int h = 150, l = 150, vitesse = 500;
	private JeuDeLaVieUI UI = new JeuDeLaVieUI(jeu);
	private JeuDeLaVieInfos infos = new JeuDeLaVieInfos(jeu);
	private JButton btnPause = new JButton("||");
	private JButton btnNext = new JButton(">>");
	private JSlider slider = new JSlider(JSlider.HORIZONTAL,1,500,250);
	private String[] regles = {"Classique", "Variante"};
	private JComboBox combo = new JComboBox(regles);
	private JFrame win = new JFrame("jeu de la vie");

	public Main() {
		jeu.initialiseGrille(l,h);
		jeu.attacheObservateur(UI);
		jeu.attacheObservateur(infos);

		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				synchronized(this) {
					isRunning = !isRunning;
					btnPause.setText(isRunning ? "||" : ">");
					btnNext.setEnabled(isRunning ? false: true);
				}
			}
		});

		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				synchronized(this) {
					jeu.calculerGenerationSuivante(mode);
				}
			}
		});

		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				vitesse = 500-slider.getValue();
			}
		});

		combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mode = mode.equals("classique") ? "variante" : "classique";
			}
		});
		
	    win.setSize(6*l, 6*h);
	    win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UI.add(btnPause);
		btnNext.setEnabled(false);
		UI.add(btnNext);
		UI.add(slider);
		UI.add(combo);
		win.add(UI);
		win.setVisible(true);
	}

	public String getMode() {
		return mode;
	}

	public static void main(String[] args) {
		Main v = new Main();
		while(true) {
			synchronized(v) {
				if (v.isRunning) {
					v.jeu.calculerGenerationSuivante(v.getMode());
					try {Thread.sleep(v.vitesse);} catch (InterruptedException e) {}
				}
			}
		}	
	}
}