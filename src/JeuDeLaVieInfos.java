/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 13:44:28 CEST 2019
*
*/
public class JeuDeLaVieInfos implements Observateur {
	
	private int numGen, nbViv;
	private JeuDeLaVie jeu;
	
	public JeuDeLaVieInfos(JeuDeLaVie jeu) {
		this.jeu = jeu;
		numGen = 0;
		nbViv = 0;
	}
	
	public void actualise() {
		
		for (int y=0; y<jeu.getYMax(); y++) {
			for (int x=0; x<jeu.getXMax(); x++) {
				if (jeu.getGrille(x,y).estVivante())
					nbViv++;
			}	
		}
		
		System.out.println("génération "+numGen+", "+nbViv+" cellules vivantes");
		
		numGen++;
		nbViv = 0;
	}
}