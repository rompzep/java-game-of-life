/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 12:51:55 CEST 2019
*
*/
public abstract class Visiteur {
	
	protected JeuDeLaVie jeu;

	public Visiteur(JeuDeLaVie jeu) {
		this.jeu = jeu;
	}

	public abstract void visiteCelluleVivante(Cellule c);

	public abstract void visiteCelluleMorte(Cellule c);
}