/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:55:07 CET 2019
*
*/
public interface Observable {
	public void attacheObservateur(Observateur o);
	public void detacheObservateur(Observateur o);
	public void notifieObservateurs();
}