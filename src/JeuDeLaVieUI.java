/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 11:07:54 CET 2019
*
*/
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class JeuDeLaVieUI extends JPanel implements Observateur {
	private JeuDeLaVie jeu;

	public JeuDeLaVieUI(JeuDeLaVie jeu) {
		this.jeu = jeu;
	}

	public void actualise() {
		repaint();
	}

	public void paint(Graphics g) {
		super.paint(g);
		int size = 5;
		for (int x=0; x<jeu.getXMax(); x++)
			for (int y=0; y<jeu.getYMax(); y++)
				if (jeu.getGrille(x,y).estVivante())
					g.fillOval(x*size, y*size, size, size);
	}
}