/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:50:25 CET 2019
*
*/
import java.lang.Math;
import java.util.ArrayList;

public class JeuDeLaVie implements Observable {
	private ArrayList<Observateur> obs = new ArrayList<Observateur>();
	private ArrayList<Commande> cmds = new ArrayList<Commande>();
	private VisiteurClassique classique = new VisiteurClassique(this);
	private VisiteurNouvellesRegles variante = new VisiteurNouvellesRegles(this);
	private Cellule[][] grille;
	private int xMax;
	private int yMax;

	public void attacheObservateur(Observateur o) {
		obs.add(o);
	}
	public void detacheObservateur(Observateur o) {
		obs.remove(o);
	}
	public void notifieObservateurs() {
		for (Observateur o : obs)
			o.actualise();
	}

	public void ajouteCommande(Commande c) {
		cmds.add(c);
	}

	public void executeCommandes() {
		for (Commande c : cmds)
			c.executer();
		cmds.clear();
	}

	public void distribueVisiteur(Visiteur v) {
		for (Cellule[] cells : grille)
			for (Cellule cell : cells)
				cell.accepte(v);
	}
	
	public void calculerGenerationSuivante(String mode) {
		distribueVisiteur(mode.equals("classique") ? classique : variante);
		executeCommandes();
		notifieObservateurs();
	}
	
	public void initialiseGrille(int xMax, int yMax) {
		this.xMax = xMax;
		this.yMax = yMax;
		this.grille = new Cellule[yMax][xMax];
		
		for (int y=0; y<yMax; y++) {
			for (int x=0; x<xMax; x++) {
				if (Math.random() < 0.5)
					grille[y][x] = new Cellule(x,y,CelluleEtatVivant.getInstance());
				else
					grille[y][x] = new Cellule(x,y,CelluleEtatMort.getInstance());
			}	
		}
	}

	public int getXMax() {
		return xMax;
	}

	public int getYMax() {
		return yMax;
	}

	public Cellule getGrille(int x, int y) {
		return grille[y][x];
	}

	public String toString() {
		String str = "";
		for (int y=0; y<yMax; y++) {
			for (int x=0; x<xMax; x++) {
				str += grille[y][x].toString() + " ";
			}
			str += "\n";	
		}
		return str;
	}
}