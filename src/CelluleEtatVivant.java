/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:16:28 CET 2019
*
*/
public class CelluleEtatVivant implements CelluleEtat {
	private static CelluleEtat singleton = new CelluleEtatVivant();

	private CelluleEtatVivant() {}
	
	public static CelluleEtat getInstance() {
		return singleton;
	}
	
	public CelluleEtat vit() {
		return this;
	}
	
	public CelluleEtat meurt() {
		return CelluleEtatMort.getInstance();
	}
	
	public Boolean estVivante() {
		return true;
	}

	public void accepte(Visiteur v, Cellule c) {
		v.visiteCelluleVivante(c);
	}
}