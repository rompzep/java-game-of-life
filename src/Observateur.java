/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:57:16 CET 2019
*
*/
public interface Observateur {
	public void actualise();
}