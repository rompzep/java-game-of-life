/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:14:56 CET 2019
*
*/
public interface CelluleEtat {
	public CelluleEtat vit();
	public CelluleEtat meurt();
	public Boolean estVivante();
	public void accepte(Visiteur v, Cellule c);
}