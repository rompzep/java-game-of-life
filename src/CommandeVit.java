/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 12:35:49 CEST 2019
*
*/
public class CommandeVit extends Commande {
		
	public CommandeVit(Cellule cell) {
		this.cell = cell;
	}

	public void executer() {
		cell.vit();
	}
}